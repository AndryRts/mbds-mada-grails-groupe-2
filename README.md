# Grails MBDS Mada Groupe 2

# Membres du groupe 2 :
- RAKOTOMANANA Kevin Elliot n°26
- RATSIMBAZAFY Andry Patrick n°52
- ANDRIAMANALINA Ranto Herizo n°2 
- RAKOTONDRAZAKA Yves Herilanja n°28
- RAFANOMEZANTSOA Fanilo Nomen'Aina n°16

# Compte:
- Login: admin
- Password: password 
# Backoffice :
- Ajout template sur les pages
- Pages protégées par un système d'authentification (login et mdp)
- Sécurisation des pages suivant les roles
# Annonce :
- Liste des annonces
- Creation, Edition et modification des annonces
# Utilisateur: 
- Liste des utilisateurs
- Création, édition et suppresion des utilisateurs

# API Rest :
- La collection est dans le fichier JSON 
- Annonce(Get resource, Get collection, Post, Put, Delete)
- User(Get resource, Get collection, Post, Put, Delete)
- Sécurisation par token et mise à place d'un système de révocation si utilisateur change de mdp ou est supprimé
